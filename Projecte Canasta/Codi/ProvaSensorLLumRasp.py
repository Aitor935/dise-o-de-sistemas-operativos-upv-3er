import spidev
import RPi.GPIO as GPIO
import time
 
GPIO.setmode(GPIO.BCM)
spi = spidev.SpiDev()
spi.open(0,0)
 
def leerADC(adcnum):
if ((adcnum &gt; 7) or (adcnum &lt; 0)):
  return -1
r = spi.xfer2([1,(8+adcnum)&lt;&lt;4,0])
adcout = ((r[1]&amp;3) &lt;&lt;8) + r[2]
return adcout
 
while True:
  print "LDR 1:" + str(leerADC(0))
  print "LDR 2:" + str(leerADC(1))
  time.sleep(1)