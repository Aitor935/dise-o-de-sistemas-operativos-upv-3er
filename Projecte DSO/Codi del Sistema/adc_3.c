#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <wiringPiSPI.h>
#include <wiringPi.h>



	
	/* Funció per a calcular el temps del bucle while de partida(), calcula els segons que dura la partida */
	double timeval_diff(struct timeval *a, struct timeval *b){
	
	return
		(double)(a->tv_sec + (double)a->tv_usec/1000000) -
		(double)(b->tv_sec + (double)b->tv_usec/1000000);
	}

	/*Funció per a llegir els valors analogics dels leds del MCP3008*/
	int analogRead(int pin){
		int ADC=-1;
		if((pin>=0)&&(pin<=7)){
			int ce = 0;
			unsigned char ByteSPI[7];
			// Cárreguem les dades
			ByteSPI[0] = 0b01;//L'ultim bit es el start
			ByteSPI[1]=(0x80)|(pin<<4);//4 primers bits de configuració
			ByteSPI[2]=0;//Byte on s'escriuen els ultims 8 bits.
			wiringPiSPIDataRW (ce, ByteSPI, 3);//Enviem la trama
			usleep(10);//Esperem 20 microsegons

			ADC=((ByteSPI[1]&0x03)<<8)|ByteSPI[2];//Tractem les dades
		}
		return (ADC);
	}

	/*Funció per a jugar una partida*/
	void iniciJoc(void){
		
		wiringPiSetup(); //Setup de la llibreria WiringPI
		int pin = 0;
		pinMode(pin, OUTPUT);
		digitalWrite(pin, LOW);
		int contadorCanastes = 0; //Contador de pasades per la barrera de leds
		//int recordCanastes = 0;
		int valorADCSensor1; //Valor llegit del ADC MCP3008
		struct timeval t_ini, t_fin; //Punts d'inici i fi de temps, per a calcular el temps de partida
		int secs; //Nombre de segons que es vol que dure una partida
		gettimeofday(&t_ini, NULL); //Inici del temps de la partida
	
		printf("Comença la partida! \n");
		printf("------------------------------- \n");

		/*Bucle while que simula la partida*/
		while(1){
			
			/*Llegim el valor del primer canal del ADC*/
			valorADCSensor1=analogRead(0);//Leemos canal 0
					
			/*Funció que determina si s'ha tallat la barrera de leds o no, si es talla, augmenta contadorCanastes*/
			
			if(valorADCSensor1 < 10){
				contadorCanastes++;
				digitalWrite(pin, HIGH);
				delay(300);
			}

			digitalWrite(pin, LOW);
			
			printf("Valor ADC1: %d\n",valorADCSensor1);
			printf("Num. Canastes: %d\n",contadorCanastes);
			delay(50);
		
			gettimeofday(&t_fin, NULL); //Fi del temps de partida
			secs = timeval_diff(&t_fin,&t_ini); //Calculem el temps de partida
			
			/*Si el temps de partida es el que volem, s'acaba la partida*/
			if(secs == 30){

				/*if(contadorCanastes > recordCanastes){
					contadorCanastes = recordCanastes;
					printf("Enhorabona, has superat el record de canastes! \n");
				}else if(contadorCanastes == recordCanastes){
					printf("Molt bé, has igualat el record de canastes! \n");
				}else{printf("Ohhhh, no has superat el record, torna a intentar-ho! \n");}*/

				printf("---------------------------------------- \n");
				printf("Numero canastes: %d\n", contadorCanastes);
				printf("Temps de partida: %d\n",secs);
				printf("S'ha acabat el temps de joc! \n");

				printf("----------------------------------------- \n");
				printf("Si vols jugar altra partida, prem el botó \n");
				printf("----------------------------------------- \n");

				system("sudo ./provaBoto");

				break;

			}
		}
	
	
	}	
	
	
	int main (void) {

		/*Carreguem el spi de la rasp*/
		system("gpio load spi");
		if (wiringPiSPISetup (0, 500000) < 0){//Definim la conexio a 0.5 Mhz
			fprintf (stderr, "Imposible abrir el dispositivo SPI 0: %s\n", strerror (errno)) ;
			exit (1) ;
		}
	
		iniciJoc();

		return 0;
			
	}


